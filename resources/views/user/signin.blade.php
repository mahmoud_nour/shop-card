@extends('layouts.master')
@section('content')
	{{-- expr --}}
<div class="row">
	<div class="col-md-4 col-md-offset-4">
		<h1>Sign In</h1>
		@if (count($errors) > 0)
	 		<alert class="alert-danger">
				@foreach ($errors->all() as $error)
					<p>{{ $error }}</p>
				@endforeach
			</alert>
		@endif
<form action="{{ route('user.signin') }}" method="post">
<div class="form-group">
	<label for="email">E-Mail</label>
	<input type="text" name="email" id="email" class="form-control">
</div>
<div class="form-group">
	<label for="password">Password</label>
	<input type="password" name="password" id="password" class="form-control">
</div>
<button class="btn btn-primary">Sign In</button>
{{ csrf_field() }}
<h5><a href="{{ route('user.signup') }}">Don't have an account? <strong>Sign up</strong></a></h5>
</form>
	</div>

</div>
@endsection
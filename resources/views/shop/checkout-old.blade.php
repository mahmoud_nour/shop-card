@extends('layouts.master')

@section('title')
	Shopping cart
@endsection

@section('content')
<div class="row">
	<div class="col-sm-6 col-md-4 col-sm-offset-4 col-md-offset-3">
		<h1>checkout</h1>
		<h4> your Total: ${{ $total }} </h4>
		<div id="charge-error" class="alert alert-danger {{ !Session::has('error') ? 'hidden' : '' }}">{{ Session::get('error') }}
		</div>
		<form action="{{ route('checkout') }}" method="post" id="checkout-form">
			<div class="row">
				<div class="col-xs-12">
					<div class="form-group">
						<label for="name">Name</label>
						<input type="text" id="name" class="form-control" required>
					</div>
				</div>
				<div class="col-xs-12">
					<div class="form-group">
						<label for="address">Address</label>
						<input type="text" id="address" class="form-control" required>
					</div>
				</div>
				<div class="col-xs-12">
					<div class="form-group">
						<label for="card-name">Card Holder Name</label>
						<input type="text" id="card-name" class="form-control" required>
					</div>
				</div>
				<div class="col-xs-12">
					<div class="form-group">
						<label for="card-number">Cridt Card Number</label>
						<input type="text" id="card-number" class="form-control" required>
					</div>
				</div>
				<div class="col-xs-12">
					<div class="row">
						<div class="col-xs-6">
					<div class="form-group">
						<label for="card-expiry-month">Expiration month</label>
						<input type="text" id="card-expiry-month" class="form-control" required>
					</div>
						</div>
						<div class="col-xs-6">
					<div class="form-group">
						<label for="card-expiry-year">Expiration year</label>
						<input type="text" id="card-expiry-year" class="form-control" required>
					</div>
						</div>
					</div>
				</div>
				<div class="col-xs-12">
					<div class="form-group">
						<label for="card-cvc">cvc</label>
						<input type="text" id="card-cvc" class="form-control" required>
					</div>
				</div>
			</div>
			{{-- {{ csrf_field() }} --}}
			<div type="submit" class="btn btn-success">Buy now</div>
		</form>
	</div>
</div>
@endsection

@section('scripts')
	<script src="https://js.stripe.com/v2/"></script>
	<script src="{{ url('src/js/checkout.js') }}"></script>
@endsection
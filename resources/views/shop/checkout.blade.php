@extends('layouts.master')
@section('title')Shopping cart @endsection
@section('style')
<style>
	.StripeElement {
	  background-color: white;
	  padding: 10px 12px;
	  border-radius: 4px;
	  border: 1px solid transparent;
	  box-shadow: 0 1px 3px 0 #e6ebf1;
	  -webkit-transition: box-shadow 150ms ease;
	  transition: box-shadow 150ms ease;
	}

	.StripeElement--focus {
	  box-shadow: 0 1px 3px 0 #cfd7df;
	}

	.StripeElement--invalid {
	  border-color: #fa755a;
	}

	.StripeElement--webkit-autofill {
	  background-color: #fefde5 !important;
	}
</style>
@endsection
@section('content')
<div class="row">
	<div class="col-sm-6 col-md-4 col-sm-offset-4 col-md-offset-3">
		<h1>checkout</h1>
		<h4> your Total: ${{ $total }} </h4>
		<div id="charge-error" class="alert alert-danger {{ !Session::has('error') ? 'hidden' : '' }}">{{ Session::get('error') }}
		</div>
		<!--Form-->
		<form action="{{ route('checkout') }}" method="post" id="payment-form">
		  <div class="form-row">
		    <label for="card-element">
		      Credit or debit card
		    </label>
		    <div id="card-element">
		      <!-- a Stripe Element will be inserted here. -->
		    </div>
		    <!-- Used to display form errors -->
		    <div id="card-errors" role="alert"></div>
		  </div>
			{{-- {{ csrf_field() }} --}}
			<input type="hidden" name="_token" value="{{ csrf_token() }}">
		  <button>Submit Payment</button>
		</form>
{{-- 		<form action="{{ route('checkout') }}" method="post" id="checkout-form">
			<div class="row">
				<div class="col-xs-12">
					<div class="form-group">
						<label for="name">Name</label>
						<input type="text" id="name" class="form-control" required>
					</div>
				</div>
				<div class="col-xs-12">
					<div class="form-group">
						<label for="address">Address</label>
						<input type="text" id="address" class="form-control" required>
					</div>
				</div>
				<div class="col-xs-12">
					<div class="form-group">
						<label for="card-name">Card Holder Name</label>
						<input type="text" id="card-name" class="form-control" required>
					</div>
				</div>
				<div class="col-xs-12">
					<div class="form-group">
						<label for="card-number">Cridt Card Number</label>
						<input type="text" id="card-number" class="form-control" required>
					</div>
				</div>
				<div class="col-xs-12">
					<div class="row">
						<div class="col-xs-6">
					<div class="form-group">
						<label for="card-expiry-month">Expiration month</label>
						<input type="text" id="card-expiry-month" class="form-control" required>
					</div>
						</div>
						<div class="col-xs-6">
					<div class="form-group">
						<label for="card-expiry-year">Expiration year</label>
						<input type="text" id="card-expiry-year" class="form-control" required>
					</div>
						</div>
					</div>
				</div>
				<div class="col-xs-12">
					<div class="form-group">
						<label for="card-cvc">cvc</label>
						<input type="text" id="card-cvc" class="form-control" required>
					</div>
				</div>
			</div>
			{{ csrf_field() }}
			<div typ="submit" class="btn btn-success">Buy now</div>
		</form> --}}
	</div>
</div>
@endsection

@section('scripts')
<script src="https://js.stripe.com/v3/"></script>
{{-- <script src="https://js.stripe.com/v2/"></script> --}}
<script>
// Create a Stripe client
var stripe = Stripe('pk_test_KQyAEKCEH4ZAuYkJvwutV5iT');
// Create an instance of Elements
var elements = stripe.elements();

// Custom styling can be passed to options when creating an Element.
// (Note that this demo uses a wider set of styles than the guide below.)
var style = {
  base: {
    color: '#32325d',
    lineHeight: '18px',
    fontFamily: '"Helvetica Neue", Helvetica, sans-serif',
    fontSmoothing: 'antialiased',
    fontSize: '16px',
    '::placeholder': {
      color: '#aab7c4'
    }
  },
  invalid: {
    color: '#fa755a',
    iconColor: '#fa755a'
  }
};

// Create an instance of the card Element
var card = elements.create('card', {style: style});

// Add an instance of the card Element into the `card-element` <div>
card.mount('#card-element');

// Handle real-time validation errors from the card Element.
card.addEventListener('change', function(event) {
  var displayError = document.getElementById('card-errors');
  if (event.error) {
    displayError.textContent = event.error.message;
  } else {
    displayError.textContent = '';
  }
});
// Handle form submission
var form = document.getElementById('payment-form');

form.addEventListener('submit', function(event) {
  event.preventDefault();

  stripe.createToken(card).then(function(result) {
    if (result.error) {
      // Inform the user if there was an error
      var errorElement = document.getElementById('card-errors');
      errorElement.textContent = result.error.message;
    } else {
      // Send the token to your server
      stripeTokenHandler(result.token);
    }
  });
});
function stripeTokenHandler(token) {
	// console.log(token);
  // Insert the token ID into the form so it gets submitted to the server
  var form = document.getElementById('payment-form');
  var hiddenInput = document.createElement('input');
  hiddenInput.setAttribute('type', 'hidden');
  hiddenInput.setAttribute('name', 'stripeToken');
  hiddenInput.setAttribute('value', token.id);
  form.appendChild(hiddenInput);
// console.log(form);
  // Submit the form
  form.submit();
}
</script>
{{-- <script src="{{ url('src/js/checkout.js') }}"></script> --}}
@endsection
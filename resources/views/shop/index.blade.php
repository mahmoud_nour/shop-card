@extends('layouts.master')
@section('title')
	shopping card
@endsection

@section('content')
	@if (Session::has('success'))
		{{-- expr --}}
	<div class="row">
		<div class="col-sm-6 col-md-4 col-md-offset-4 col-sm-offset-3">
			<div id="charge-message" class="alert alert-success">
				{{ Session::get('success') }}
			</div>
		</div>
	</div>
	@endif
<div class="row">
	<div class="container">
	@foreach ($products as $product)
	  <div class="col-sm-6 col-md-4">
		{{-- expr --}}
		<div class="thumbnail">
		  <img src="{{ $product->imagePath }}" alt="..." class="img-responsive">
		  <div class="caption ">
		    <h3 class="title">{{ $product->title }}</h3>
		    <p class="description">{{$product->description}}</p>
		   <div class="clearfix">
		   	<div class="price pull-left">{{ $product->price }}</div>
		   	 <p><a href="{{ route('product.addToCart',['id' => $product->id]) }}" class="btn btn-success pull-right" role="button">add to card</a></p>
		   </div>
		  </div>
		</div>
		{{-- expr --}}
	  </div>
	@endforeach
	</div>
</div>



@endsection
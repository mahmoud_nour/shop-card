<?php

namespace App;

class Cart {
	public $items = null;
	public $totalQty = 0;
	public $totalPrice = 0;

	public function __construct($oldCart) {
		if ($oldCart) {
			$this->items = $oldCart->items;
			$this->totalQty = $oldCart->totalQty;
			$this->totalPrice = $oldCart->totalPrice;
		}
	}
	public function add($item, $id) {
		$storedIdItem = ['qty' => 0, 'price' => $item->price, 'item' => $item];
		if ($this->items) {
			if (array_key_exists($id, $this->items)) {
				$storedIdItem = $this->items[$id];
			}
		}

		$storedIdItem['qty']++;
		$storedIdItem['price'] = $item->price * $storedIdItem['qty'];
		$this->items[$id] = $storedIdItem;
		$this->totalQty++;
		$this->totalPrice += $item->price;
	}
}

<?php

Route::get('/', 'ProductController@getIndex')->name('product.index');

Route::get('/add-to-cart/{id}', 'ProductController@getAddToCart')->name('product.addToCart');
Route::get('/shopping-cart', 'ProductController@getCart')->name('product.shoppingCart');

Route::get('/checkout', 'ProductController@getCheckout')->name('checkout');
Route::post('/checkout', 'ProductController@postCheckout')->name('checkout');

Route::group(['prefix' => 'user'], function () {
	//
	route::group(['middleware' => 'guest'], function () {

		Route::get('/signup', 'UserController@getSignup')->name('user.signup');
		Route::post('/signup', 'UserController@postSignup')->name('user.signup');

		Route::get('/signin', 'UserController@getSignin')->name('user.signin');
		Route::post('/signin', 'UserController@postSignin')->name('user.signin');
		Route::get('/login', ['as' => 'login', 'uses' => 'UserController@getSignin']);
	});

	route::group(['middleware' => 'auth'], function () {
		Route::get('/profile', 'UserController@getProfile')->name('user.profile');
		Route::get('/logout', 'UserController@getLogout')->name('user.logout');
	});

});

// Route::get('/', [
// 	'uses' => 'ProductController@getIndex',
// 	'as' => 'product.index',
// ]);

// Route::get('/signup', [
// 	'uses' => 'UserController@getSignup',
// 	'as' => 'user.signup',
// ]);
// Route::post('/signup', [
// 	'uses' => 'UserController@postSignup',
// 	'as' => 'user.signup',
// ]);